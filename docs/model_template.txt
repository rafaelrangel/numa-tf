NEUTRAL-FORMAT FILE
This file stores model and analysis data for the NUMA-TF program.
To modify the properties, edit only the data below the %TAGS.
All units must be consistent.

-----------------------------------------------------------------------
Model description (optional)
%HEADER



============================== MODEL DATA ==============================



-----------------------------------------------------------------------
Analysis model type: 'TRUSS2D', 'FRAME2D', 'TRUSS3D', 'FRAME3D'
%MODEL.ANALYSIS_MODEL


-----------------------------------------------------------------------
Node coordinates
First line: Total number of nodes
Following lines: Node ID, coord_X, coord_Y, coord_Z
%MODEL.NODE_COORD


-----------------------------------------------------------------------
Support conditions (all unlisted nodes are assumed to be completely free)
First line: Total number of nodes with support
Following lines: Node ID, supp_DX, supp_DY, supp_DZ, supp_RX, supp_RY, supp_RZ
supp = 0 -> Free degree-of-freedom
supp = 1 -> Fixed degree-of-freedom
%MODEL.NODE_SUPPORT


-----------------------------------------------------------------------
Nodal prescribed displacements
First line: Total number of nodes with prescribed displacement
Following lines: Node ID, DX, DY, DZ, RX, RY, RZ
%MODEL.NODE_PRESC_DISPL


-----------------------------------------------------------------------
Initial conditions
First line: Total number of nodes with initial condition
Following lines: Node ID,
                 dDX, dDY, dDZ, dRX, dRY, dRZ,
                 vDX, vDY, vDZ, vRX, vRY, vRZ,
                 aDX, aDY, aDZ, aRX, aRY, aRZ
%MODEL.INIT_CONDITION


-----------------------------------------------------------------------
Materials properties
First line: Total number of materials
Following lines: Material ID, E, v, rho, alpha
%MODEL.MATERIAL_PROPERTY


-----------------------------------------------------------------------
Cross-sections properties
First line: Total number of cross-sections
Following lines: Cross-section ID, Ax, Ay, Az, Ix, Iy, Iz, Hy, Hz
%MODEL.SECTION_PROPERTY


-----------------------------------------------------------------------
Elements
First line: Total number of elements
Following lines: Element ID,
                 Element Type (1=Euler-Bernoulli,2=Timoshenko),
                 Material ID, Cross-section ID,
                 Init. Node ID, Final Node ID,
                 Init. End fixity, Final End fixity (0=hinged,1=fixed),
                 vz_X, vz_Y, vz_Z
%MODEL.ELEMENT


-----------------------------------------------------------------------
Nodal loads
First line: Total number of nodes with applied load
Following lines: Node ID, FX, FY, FZ, MX, MY, MZ
%MODEL.LOAD.NODE_LOAD


-----------------------------------------------------------------------
Element uniform distributed loads
First line: Total number of elements with uniform distributed load
Following lines: Element ID, Load Direction (0=Global,1=Local), QX, QY, QZ
%MODEL.LOAD.ELEM_UNIFORM


-----------------------------------------------------------------------
Element linear distributed loads
First line: Total number of elements with linear distributed load
Following lines: Element ID, Load Direction (0=Global,1=Local), QX1, QY1, QZ1, QX2, QY2, QZ2
%MODEL.LOAD.ELEM_LINEAR


-----------------------------------------------------------------------
Element thermal loads
First line: Total number of elements with thermal load
Following lines: Element ID, DTX, DTY, DTZ
%MODEL.LOAD.ELEM_THERMAL



============================= ANALYSIS DATA =============================



-----------------------------------------------------------------------
Analysis type: 'LINEAR_ELASTIC', 'NONLINEAR_GEOM', 'VIBRATION_FREE',
               'MODAL_BUCKLING', 'MODAL_VIBRATION'
%ANALYSIS.TYPE


-----------------------------------------------------------------------
Tangent stiffness matrix formulation: 'CR', 'UL'
%ANALYSIS.TANGENT_STIFFNESS


-----------------------------------------------------------------------
Mass distribution type: 'CONSISTENT', 'LUMPED'
%ANALYSIS.MASS_MATRIX


-----------------------------------------------------------------------
Damping type: 'UNDAMPED', 'PROPORTIONAL'
%ANALYSIS.DAMPING


-----------------------------------------------------------------------
Solution method for nonlinear analysis:
'EULER', 'LCM', 'WCM', 'ALCM_FNP', 'ALCM_UNP',
'ALCM_CYL', 'ALCM_SPH', 'MNCM', 'ORCM', 'GDCM'
%ANALYSIS.METHOD_NONLINEAR


-----------------------------------------------------------------------
Solution method for vibration analysis:
'RK4', 'NEWMARK', 'WILSONT', 'AM'
%ANALYSIS.METHOD_VIBRATION


-----------------------------------------------------------------------
Type of increment size: 'CONSTANT', 'ADJUSTED'
%ANALYSIS.INCREMENT_TYPE


-----------------------------------------------------------------------
Type of iteration strategy: 'STANDARD', 'MODIFIED'
%ANALYSIS.ITERATION_TYPE


-----------------------------------------------------------------------
Numerical parameters for nonlinear analysis:
-Increment of load ratio in the predicted solution of first step (positive value);
-Limit value of load ratio to stop analysis;
-Maximum number of steps to stop analysis (positive value);
-Maximum number of iterations in each step (positive value);
-Desired number of iterations in each step (positive value);
-Tolerance to assume that equilibrium has been reached (positive value);
%ANALYSIS.PARAMETERS_NONLINEAR


-----------------------------------------------------------------------
Numerical parameters for vibration analysis:
-Increment of time in each step (positive value);
-Total number of incremental time steps (positive value);
-Rayleigh coefficients A and B for damping matrix: [C] = A*[M] + B*[K]
%ANALYSIS.PARAMETERS_VIBRATION


-----------------------------------------------------------------------
Type of result domain: 'TIME', 'FREQUENCY'
%ANALYSIS.RESULT_DOMAIN


-----------------------------------------------------------------------
Data for plotting result curves (equilibrium path or transient response):
First line: Total number of curves
Following lines: Curve name, node ID, degree-of-freedom (1=DX,2=DY,3=DZ,4=RX,5=RY,6=RZ)
%ANALYSIS.RESULT_CURVES


-----------------------------------------------------------------------
Data for plotting buckling/vibration modes
First line: Total number of modes
Following lines: Mode ID, scale factor for plotting mode
%ANALYSIS.RESULT_MODES


%END