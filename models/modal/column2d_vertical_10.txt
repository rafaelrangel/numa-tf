NEUTRAL-FORMAT FILE
This file stores model and analysis data for the NUMA-TF program.
To modify the properties, edit only the data below the %TAGS.
All units must be consistent.

-----------------------------------------------------------------------
Model description (optional)
%HEADER
Vertical column with compression load (10 elements discretization).



============================== MODEL DATA ==============================



-----------------------------------------------------------------------
Analysis model type: 'TRUSS2D', 'FRAME2D', 'TRUSS3D', 'FRAME3D'
%MODEL.ANALYSIS_MODEL
'FRAME2D'

-----------------------------------------------------------------------
Node coordinates
First line: Total number of nodes
Following lines: Node ID, coord_X, coord_Y, coord_Z
%MODEL.NODE_COORD
11
1    0.0 0.0 0.0
2    0.0 0.1 0.0
3    0.0 0.2 0.0
4    0.0 0.3 0.0
5    0.0 0.4 0.0
6    0.0 0.5 0.0
7    0.0 0.6 0.0
8    0.0 0.7 0.0
9    0.0 0.8 0.0
10   0.0 0.9 0.0
11   0.0 1.0 0.0

-----------------------------------------------------------------------
Support conditions (all unlisted nodes are assumed to be completely free)
First line: Total number of nodes with support
Following lines: Node ID, supp_DX, supp_DY, supp_DZ, supp_RX, supp_RY, supp_RZ
supp = 0 -> Free degree-of-freedom
supp = 1 -> Fixed degree-of-freedom
%MODEL.NODE_SUPPORT
1
1     1  1  1  1  1  1

-----------------------------------------------------------------------
Materials properties
First line: Total number of materials
Following lines: Material ID, E, v, rho, alpha
%MODEL.MATERIAL_PROPERTY
1
1     1e+07  0.3  1000  1e-05

-----------------------------------------------------------------------
Cross-sections properties
First line: Total number of cross-sections
Following lines: Cross-section ID, Ax, Ay, Az, Ix, Iy, Iz, Hy, Hz
%MODEL.SECTION_PROPERTY
1
1     1e-02  1e-02  1e-02  1e-05  1e-05  1e-05  1e-01  1e-01

-----------------------------------------------------------------------
Elements
First line: Total number of elements
Following lines: Element ID,
                 Element Type (1=Euler-Bernoulli,2=Timoshenko),
                 Material ID, Cross-section ID,
                 Init. Node ID, Final Node ID,
                 Init. End fixity, Final End fixity (0=hinged,1=fixed),
				 vz_X, vz_Y, vz_Z
%MODEL.ELEMENT
10
1      1   1 1   1  2    1 1   0 0 1
2      1   1 1   2  3    1 1   0 0 1
3      1   1 1   3  4    1 1   0 0 1
4      1   1 1   4  5    1 1   0 0 1
5      1   1 1   5  6    1 1   0 0 1
6      1   1 1   6  7    1 1   0 0 1
7      1   1 1   7  8    1 1   0 0 1
8      1   1 1   8  9    1 1   0 0 1
9      1   1 1   9  10   1 1   0 0 1
10     1   1 1   10 11   1 1   0 0 1


-----------------------------------------------------------------------
Nodal loads
First line: Total number of nodes with applied load
Following lines: Node ID, FX, FY, FZ, MX, MY, MZ
%MODEL.LOAD.NODE_LOAD
1
11     0  -1  0  0  0  0



============================= ANALYSIS DATA =============================



-----------------------------------------------------------------------
Analysis type: 'LINEAR_ELASTIC', 'NONLINEAR_GEOM', 'VIBRATION_FREE',
               'MODAL_BUCKLING', 'MODAL_VIBRATION'
%ANALYSIS.TYPE
'MODAL_BUCKLING'

-----------------------------------------------------------------------
Mass distribution type: 'CONSISTENT', 'LUMPED'
%ANALYSIS.MASS_MATRIX
'CONSISTENT'

-----------------------------------------------------------------------
Data for plotting buckling/vibration modes
First line: Total number of modes
Following lines: Mode ID, scale factor for plotting mode
%ANALYSIS.RESULT_MODES
3
1   0.5
2   1.0
3   0.5


%END