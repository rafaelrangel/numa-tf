NEUTRAL-FORMAT FILE
This file stores model and analysis data for the NUMA-TF program.
To modify the properties, edit only the data below the %TAGS.
All units must be consistent.

-----------------------------------------------------------------------
Model description (optional)
%HEADER
Fixed end shallow arch with uniform distributed loads in local system of elements
(20 elements discretization).



============================== MODEL DATA ==============================



-----------------------------------------------------------------------
Analysis model type: 'TRUSS2D', 'FRAME2D', 'TRUSS3D', 'FRAME3D'
%MODEL.ANALYSIS_MODEL
'FRAME2D'

-----------------------------------------------------------------------
Node coordinates
First line: Total number of nodes
Following lines: Node ID, coord_X, coord_Y, coord_Z
%MODEL.NODE_COORD
21
1   -5.000000000000000   3.750000000000000   0.0
2   -4.631280917549946   4.196872295262001   0.0
3   -4.222767070231803   4.607682527102526   0.0
4   -3.777968655934404   4.978900765708953   0.0
5   -3.300707651477076   5.307337279604593   0.0
6   -2.795084971874739   5.590169943749474   0.0
7   -2.265445232797433   5.824968488944416   0.0
8   -1.716339419006525   6.009715384172893   0.0
9   -1.152485779542945   6.142823172446957   0.0
10  -0.578729285679638   6.223148111197156   0.0
11   0.000000000000000   6.250000000000000   0.0
12   0.578729285679627   6.223148111197157   0.0
13   1.152485779542934   6.142823172446958   0.0
14   1.716339419006515   6.009715384172896   0.0
15   2.265445232797424   5.824968488944418   0.0
16   2.795084971874730   5.590169943749477   0.0
17   3.300707651477068   5.307337279604599   0.0
18   3.777968655934396   4.978900765708959   0.0
19   4.222767070231796   4.607682527102532   0.0
20   4.631280917549940   4.196872295262007   0.0
21   5.000000000000000   3.750000000000000   0.0

-----------------------------------------------------------------------
Support conditions (all unlisted nodes are assumed to be completely free)
First line: Total number of nodes with support
Following lines: Node ID, supp_DX, supp_DY, supp_DZ, supp_RX, supp_RY, supp_RZ
supp = 0 -> Free degree-of-freedom
supp = 1 -> Fixed degree-of-freedom
%MODEL.NODE_SUPPORT
2
1      1  1  1  1  1  1
21     1  1  1  1  1  1

-----------------------------------------------------------------------
Materials properties
First line: Total number of materials
Following lines: Material ID, E, v, rho, alpha
%MODEL.MATERIAL_PROPERTY
1
1     2.1e+09  0.3  1000  1e-05

-----------------------------------------------------------------------
Cross-sections properties
First line: Total number of cross-sections
Following lines: Cross-section ID, Ax, Ay, Az, Ix, Iy, Iz, Hy, Hz
%MODEL.SECTION_PROPERTY
1
1     1e-01  1e-01  1e-01  1e-04  1e-04  1e-04  1e-01  1e-01

-----------------------------------------------------------------------
Elements
First line: Total number of elements
Following lines: Element ID,
                 Element Type (1=Euler-Bernoulli,2=Timoshenko),
                 Material ID, Cross-section ID,
                 Init. Node ID, Final Node ID,
                 Init. End fixity, Final End fixity (0=hinged,1=fixed),
                 vz_X, vz_Y, vz_Z
%MODEL.ELEMENT
20
1      1   1 1   1  2    1 1   0 0 1
2      1   1 1   2  3    1 1   0 0 1
3      1   1 1   3  4    1 1   0 0 1
4      1   1 1   4  5    1 1   0 0 1
5      1   1 1   5  6    1 1   0 0 1
6      1   1 1   6  7    1 1   0 0 1
7      1   1 1   7  8    1 1   0 0 1
8      1   1 1   8  9    1 1   0 0 1
9      1   1 1   9  10   1 1   0 0 1
10     1   1 1   10 11   1 1   0 0 1
11     1   1 1   11 12   1 1   0 0 1
12     1   1 1   12 13   1 1   0 0 1
13     1   1 1   13 14   1 1   0 0 1
14     1   1 1   14 15   1 1   0 0 1
15     1   1 1   15 16   1 1   0 0 1
16     1   1 1   16 17   1 1   0 0 1
17     1   1 1   17 18   1 1   0 0 1
18     1   1 1   18 19   1 1   0 0 1
19     1   1 1   19 20   1 1   0 0 1
20     1   1 1   20 21   1 1   0 0 1

-----------------------------------------------------------------------
Element uniform distributed loads
First line: Total number of elements with uniform distributed load
Following lines: Element ID, Load Direction (0=Global,1=Local), QX, QY, QZ
%MODEL.LOAD.ELEM_UNIFORM
20
1      1   0  -50000  0
2      1   0  -50000  0
3      1   0  -50000  0
4      1   0  -50000  0
5      1   0  -50000  0
6      1   0  -50000  0
7      1   0  -50000  0
8      1   0  -50000  0
9      1   0  -50000  0
10     1   0  -50000  0
11     1   0  -50000  0
12     1   0  -50000  0
13     1   0  -50000  0
14     1   0  -50000  0
15     1   0  -50000  0
16     1   0  -50000  0
17     1   0  -50000  0
18     1   0  -50000  0
19     1   0  -50000  0
20     1   0  -50000  0



============================= ANALYSIS DATA =============================



-----------------------------------------------------------------------
Analysis type: 'LINEAR_ELASTIC', 'NONLINEAR_GEOM', 'VIBRATION_FREE',
               'MODAL_BUCKLING', 'MODAL_VIBRATION'
%ANALYSIS.TYPE
'NONLINEAR_GEOM'

-----------------------------------------------------------------------
Tangent stiffness matrix formulation: 'CR', 'UL'
%ANALYSIS.TANGENT_STIFFNESS
'CR'

-----------------------------------------------------------------------
Solution method for nonlinear analysis
'EULER', 'LCM', 'WCM', 'ALCM_FNP', 'ALCM_UNP',
'ALCM_CYL', 'ALCM_SPH', 'MNCM', 'ORCM', 'GDCM'
%ANALYSIS.METHOD_NONLINEAR
'MNCM'

-----------------------------------------------------------------------
Type of increment size: 'CONSTANT', 'ADJUSTED'
%ANALYSIS.INCREMENT_TYPE
'ADJUSTED'

-----------------------------------------------------------------------
Type of iteration strategy: 'STANDARD', 'MODIFIED'
%ANALYSIS.ITERATION_TYPE
'STANDARD'

-----------------------------------------------------------------------
Numerical parameters for nonlinear analysis:
-Increment of load ratio in the predicted solution of first step (positive value);
-Limit value of load ratio to stop analysis;
-Maximum number of steps to stop analysis (positive value);
-Maximum number of iterations in each step (positive value);
-Desired number of iterations in each step (positive value);
-Tolerance to assume that equilibrium has been reached (positive value);
%ANALYSIS.PARAMETERS_NONLINEAR
0.01  1.0  5000  100  3  0.00001

-----------------------------------------------------------------------
Data for plotting result curves (equilibrium path or transient response):
First line: Total number of curves
Following lines: Curve name, node ID, degree-of-freedom (1=DX,2=DY,3=DZ,4=RX,5=RY,6=RZ)
%ANALYSIS.RESULT_CURVES
1
DY  11  2


%END