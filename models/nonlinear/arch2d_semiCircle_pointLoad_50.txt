NEUTRAL-FORMAT FILE
This file stores model and analysis data for the NUMA-TF program.
To modify the properties, edit only the data below the %TAGS.
All units must be consistent.

-----------------------------------------------------------------------
Model description (optional)
%HEADER
Simply supported semi-circle arch with a vertical point load at the middle
(50 elements discretization).



============================== MODEL DATA ==============================



-----------------------------------------------------------------------
Analysis model type: 'TRUSS2D', 'FRAME2D', 'TRUSS3D', 'FRAME3D'
%MODEL.ANALYSIS_MODEL
'FRAME2D'

-----------------------------------------------------------------------
Node coordinates
First line: Total number of nodes
Following lines: Node ID, coord_X, coord_Y, coord_Z
%MODEL.NODE_COORD
51
1    -0.500000000000000   0.000000000000000   0.0
2    -0.499013364214136   0.031395259764657   0.0
3    -0.496057350657239   0.062666616782152   0.0
4    -0.491143625364344   0.093690657292863   0.0
5    -0.484291580564315   0.124344943582428   0.0
6    -0.475528258147577   0.154508497187474   0.0
7    -0.464888242944126   0.184062276342339   0.0
8    -0.452413526233010   0.212889645782537   0.0
9    -0.438153340021932   0.240876837050858   0.0
10   -0.422163962751007   0.267913397489499   0.0
11   -0.404508497187473   0.293892626146237   0.0
12   -0.385256621387894   0.318711994874345   0.0
13   -0.364484313710705   0.342273552964345   0.0
14   -0.342273552964344   0.364484313710706   0.0
15   -0.318711994874344   0.385256621387895   0.0
16   -0.293892626146236   0.404508497187474   0.0
17   -0.267913397489498   0.422163962751008   0.0
18   -0.240876837050857   0.438153340021932   0.0
19   -0.212889645782536   0.452413526233010   0.0
20   -0.184062276342338   0.464888242944126   0.0
21   -0.154508497187473   0.475528258147577   0.0
22   -0.124344943582426   0.484291580564316   0.0
23   -0.093690657292861   0.491143625364345   0.0
24   -0.062666616782151   0.496057350657239   0.0
25   -0.031395259764656   0.499013364214136   0.0
26    0.000000000000000   0.500000000000000   0.0
27    0.031395259764658   0.499013364214136   0.0
28    0.062666616782153   0.496057350657239   0.0
29    0.093690657292864   0.491143625364344   0.0
30    0.124344943582429   0.484291580564315   0.0
31    0.154508497187475   0.475528258147576   0.0
32    0.184062276342340   0.464888242944125   0.0
33    0.212889645782538   0.452413526233009   0.0
34    0.240876837050859   0.438153340021931   0.0
35    0.267913397489500   0.422163962751007   0.0
36    0.293892626146238   0.404508497187473   0.0
37    0.318711994874346   0.385256621387894   0.0
38    0.342273552964345   0.364484313710705   0.0
39    0.364484313710707   0.342273552964343   0.0
40    0.385256621387896   0.318711994874344   0.0
41    0.404508497187475   0.293892626146235   0.0
42    0.422163962751008   0.267913397489497   0.0
43    0.438153340021932   0.240876837050856   0.0
44    0.452413526233010   0.212889645782535   0.0
45    0.464888242944126   0.184062276342338   0.0
46    0.475528258147577   0.154508497187472   0.0
47    0.484291580564316   0.124344943582426   0.0
48    0.491143625364345   0.093690657292861   0.0
49    0.496057350657239   0.062666616782151   0.0
50    0.499013364214136   0.031395259764655   0.0
51    0.500000000000000   0.000000000000000   0.0

-----------------------------------------------------------------------
Support conditions (all unlisted nodes are assumed to be completely free)
First line: Total number of nodes with support
Following lines: Node ID, supp_DX, supp_DY, supp_DZ, supp_RX, supp_RY, supp_RZ
supp = 0 -> Free degree-of-freedom
supp = 1 -> Fixed degree-of-freedom
%MODEL.NODE_SUPPORT
2
1      1  1  1  0  0  0
51     1  1  1  0  0  0

-----------------------------------------------------------------------
Materials properties
First line: Total number of materials
Following lines: Material ID, E, v, rho, alpha
%MODEL.MATERIAL_PROPERTY
1
1     2e+07  0.3  1000  1e-05

-----------------------------------------------------------------------
Cross-sections properties
First line: Total number of cross-sections
Following lines: Cross-section ID, Ax, Ay, Az, Ix, Iy, Iz, Hy, Hz
%MODEL.SECTION_PROPERTY
1
1     1e-03  1e-03  1e-03  1e-08  1e-08  1e-08  1e-01  1e-01

-----------------------------------------------------------------------
Elements
First line: Total number of elements
Following lines: Element ID,
                 Element Type (1=Euler-Bernoulli,2=Timoshenko),
                 Material ID, Cross-section ID,
                 Init. Node ID, Final Node ID,
                 Init. End fixity, Final End fixity (0=hinged,1=fixed),
                 vz_X, vz_Y, vz_Z
%MODEL.ELEMENT
50
1      1   1 1   1  2    1 1   0 0 1
2      1   1 1   2  3    1 1   0 0 1
3      1   1 1   3  4    1 1   0 0 1
4      1   1 1   4  5    1 1   0 0 1
5      1   1 1   5  6    1 1   0 0 1
6      1   1 1   6  7    1 1   0 0 1
7      1   1 1   7  8    1 1   0 0 1
8      1   1 1   8  9    1 1   0 0 1
9      1   1 1   9  10   1 1   0 0 1
10     1   1 1   10 11   1 1   0 0 1
11     1   1 1   11 12   1 1   0 0 1
12     1   1 1   12 13   1 1   0 0 1
13     1   1 1   13 14   1 1   0 0 1
14     1   1 1   14 15   1 1   0 0 1
15     1   1 1   15 16   1 1   0 0 1
16     1   1 1   16 17   1 1   0 0 1
17     1   1 1   17 18   1 1   0 0 1
18     1   1 1   18 19   1 1   0 0 1
19     1   1 1   19 20   1 1   0 0 1
20     1   1 1   20 21   1 1   0 0 1
21     1   1 1   21 22   1 1   0 0 1
22     1   1 1   22 23   1 1   0 0 1
23     1   1 1   23 24   1 1   0 0 1
24     1   1 1   24 25   1 1   0 0 1
25     1   1 1   25 26   1 1   0 0 1
26     1   1 1   26 27   1 1   0 0 1
27     1   1 1   27 28   1 1   0 0 1
28     1   1 1   28 29   1 1   0 0 1
29     1   1 1   29 30   1 1   0 0 1
30     1   1 1   30 31   1 1   0 0 1
31     1   1 1   31 32   1 1   0 0 1
32     1   1 1   32 33   1 1   0 0 1
33     1   1 1   33 34   1 1   0 0 1
34     1   1 1   34 35   1 1   0 0 1
35     1   1 1   35 36   1 1   0 0 1
36     1   1 1   36 37   1 1   0 0 1
37     1   1 1   37 38   1 1   0 0 1
38     1   1 1   38 39   1 1   0 0 1
39     1   1 1   39 40   1 1   0 0 1
40     1   1 1   40 41   1 1   0 0 1
41     1   1 1   41 42   1 1   0 0 1
42     1   1 1   42 43   1 1   0 0 1
43     1   1 1   43 44   1 1   0 0 1
44     1   1 1   44 45   1 1   0 0 1
45     1   1 1   45 46   1 1   0 0 1
46     1   1 1   46 47   1 1   0 0 1
47     1   1 1   47 48   1 1   0 0 1
48     1   1 1   48 49   1 1   0 0 1
49     1   1 1   49 50   1 1   0 0 1
50     1   1 1   50 51   1 1   0 0 1

-----------------------------------------------------------------------
Nodal loads
First line: Total number of nodes with applied load
Following lines: Node ID, FX, FY, FZ, MX, MY, MZ
%MODEL.LOAD.NODE_LOAD
1
26     0  -1000  0  0  0  0



============================= ANALYSIS DATA =============================



-----------------------------------------------------------------------
Analysis type: 'LINEAR_ELASTIC', 'NONLINEAR_GEOM', 'VIBRATION_FREE',
               'MODAL_BUCKLING', 'MODAL_VIBRATION'
%ANALYSIS.TYPE
'NONLINEAR_GEOM'

-----------------------------------------------------------------------
Tangent stiffness matrix formulation: 'CR', 'UL'
%ANALYSIS.TANGENT_STIFFNESS
'CR'

-----------------------------------------------------------------------
Solution method for nonlinear analysis
'EULER', 'LCM', 'WCM', 'ALCM_FNP', 'ALCM_UNP',
'ALCM_CYL', 'ALCM_SPH', 'MNCM', 'ORCM', 'GDCM'
%ANALYSIS.METHOD_NONLINEAR
'MNCM'

-----------------------------------------------------------------------
Type of increment size: 'CONSTANT', 'ADJUSTED'
%ANALYSIS.INCREMENT_TYPE
'ADJUSTED'

-----------------------------------------------------------------------
Type of iteration strategy: 'STANDARD', 'MODIFIED'
%ANALYSIS.ITERATION_TYPE
'STANDARD'

-----------------------------------------------------------------------
Numerical parameters for nonlinear analysis:
-Increment of load ratio in the predicted solution of first step (positive value);
-Limit value of load ratio to stop analysis;
-Maximum number of steps to stop analysis (positive value);
-Maximum number of iterations in each step (positive value);
-Desired number of iterations in each step (positive value);
-Tolerance to assume that equilibrium has been reached (positive value);
%ANALYSIS.PARAMETERS_NONLINEAR
0.01  -0.40  2000  100  2  0.0001

-----------------------------------------------------------------------
Data for plotting result curves (equilibrium path or transient response):
First line: Total number of curves
Following lines: Curve name, node ID, degree-of-freedom (1=DX,2=DY,3=DZ,4=RX,5=RY,6=RZ)
%ANALYSIS.RESULT_CURVES
1
DY  26  2


%END